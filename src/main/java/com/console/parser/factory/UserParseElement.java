package com.console.parser.factory;

import com.console.parser.entity.User;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;

public interface UserParseElement {
    void parse(XMLEventReader xmlEventReader, User user) throws XMLStreamException;
}
