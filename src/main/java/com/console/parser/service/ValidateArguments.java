package com.console.parser.service;

import com.console.parser.constance.SearchArguments;
import com.console.parser.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@Service
public class ValidateArguments {
    public List<Predicate<User>> getSearchParameters(final String[] args) {
        final List<Predicate<User>> filterList = new ArrayList<>();
        for (String s : args) {
            filterList.add(parseArguments(s));
        }
        return filterList;
    }

    private Predicate<User> parseArguments(final String attribute) {
        final String regexReplaceNameAttribute = ".*=";
        final String regexReplaceAttributeValue = "=.*";
        final String nameAttribute = attribute.replaceAll(regexReplaceAttributeValue, "");
        final String value = attribute.replaceAll(regexReplaceNameAttribute, "");
        return checkAttribute(nameAttribute, value);
    }

    private Predicate<User> checkAttribute(final String nameAttribute, final String value) {
        if (nameAttribute.equals(SearchArguments.AGE)) {
            return x -> x.getAge() == Integer.parseInt(value);
        } else if (nameAttribute.equals(SearchArguments.EMAIL)) {
            return x -> x.getEmail().equals(value);
        } else if (nameAttribute.equals(SearchArguments.NAME)) {
            return x -> x.getName().equals(value);
        } else if (nameAttribute.equals(SearchArguments.MIN_AGE)) {
            return x -> x.getAge() > Integer.parseInt(value);
        } else if (nameAttribute.equals(SearchArguments.MAX_AGE)) {
            return x -> x.getAge() < Integer.parseInt(value);
        } else {
            return null;
        }
    }
}
