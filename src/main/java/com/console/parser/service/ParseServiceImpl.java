package com.console.parser.service;

import com.console.parser.entity.User;
import com.console.parser.factory.UserParseElement;
import com.console.parser.factory.UserParseFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stax.StAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Log4j2
public class ParseServiceImpl implements ParseService {
    private static final String VALIDATE_SCHEMA = "user.xsd";

    /**
     * @see ParseService#parse(String)
     */
    @Override
    public List<User> parse(final String fileName) {
        validateDocument(fileName);
        return parseFile(fileName);
    }

    /**
     * @see ParseService#parse(String, List)
     */
    @Override
    public List<User> parse(final String fileName, final List<Predicate<User>> filterAttributes) {
        log.debug("Start parse xml file with filter attributes");
        final List<User> parse = parse(fileName);
        return parse.stream()
                .filter(filterAttributes.stream().reduce(x -> true, Predicate::and))
                .collect(Collectors.toList());
    }

    private List<User> parseFile(final String fileName) {
        log.debug("Start parse xml file");
        final List<User> resultList = new ArrayList<>();
        final XMLInputFactory xf = XMLInputFactory.newInstance();
        try {
            final XMLEventReader xmlEventReader = xf.createXMLEventReader(new FileInputStream(fileName));
            while (xmlEventReader.hasNext()) {
                Optional<User> user = parseUser(xmlEventReader);
                if (validateUser(user)) {
                    user.ifPresent(resultList::add);
                }
            }
        } catch (FileNotFoundException e) {
            log.info("File " + fileName + " not found!");
        } catch (XMLStreamException e) {
            log.info("Get xml stream from file " + fileName + " throw error! " + e.getMessage());
        }
        return resultList;
    }

    private boolean validateUser(final Optional<User> user) {
        return user.filter(value -> value.getId() != null).isPresent();
    }

    private Optional<User> parseUser(final XMLEventReader xmlEventReader) throws XMLStreamException {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        final User user = new User();
        while (!checkElementOnEnd(xmlEvent)) {
            if (xmlEvent.isStartElement()) {
                final StartElement startElement = xmlEvent.asStartElement();
                final String elementName = startElement.getName().getLocalPart();
                final Optional<UserParseElement> userParseElement = UserParseFactory.getUserParseElement(elementName);
                if (userParseElement.isPresent()) {
                    userParseElement.get().parse(xmlEventReader, user);
                }
            }
            xmlEvent = xmlEventReader.nextEvent();
        }
        log.debug("Get new user from file");
        return Optional.of(user);
    }

    private boolean checkElementOnEnd(final XMLEvent xmlEvent) {
        if (xmlEvent.isEndElement()) {
            final EndElement endElement = xmlEvent.asEndElement();
            final String elementName = endElement.getName().getLocalPart();
            return elementName.equals("user");
        } else {
            return xmlEvent.isEndDocument();
        }
    }

    private void validateDocument(final String fileName) {
        final XMLStreamReader reader;
        try {
            reader = XMLInputFactory.newInstance().createXMLStreamReader(
                    new FileInputStream(fileName));

            final SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            final Schema schema = factory.newSchema(new File(VALIDATE_SCHEMA));

            final Validator validator = schema.newValidator();
            validator.validate(new StAXSource(reader));
        } catch (FileNotFoundException e) {
            log.info("File not found " + fileName);
        } catch (IOException e) {
            log.info("File " + fileName + " not found!");
        } catch (XMLStreamException e) {
            log.info("Error get xml stream from file " + fileName + " " + e.getMessage());
        } catch (SAXException e) {
            log.info("Parse file " + fileName + " error " + e.getMessage());
        }
        log.debug("File validation success!");
    }
}
