package com.console.parser.constance;

public class SearchArguments {
    public static final String MIN_AGE = "minAge";
    public static final String MAX_AGE = "maxAge";
    public static final String AGE = "age";
    public static final String EMAIL = "email";
    public static final String NAME = "name";
    private SearchArguments() {
    }
}
